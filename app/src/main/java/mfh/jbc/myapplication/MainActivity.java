package mfh.jbc.myapplication;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

// ctrl+shift+enter = new line and add comma;
//
public class MainActivity extends AppCompatActivity {

    private final String DEBUG = "TEST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(DEBUG, "onCreate");


        Button btn = findViewById(R.id.btn_login);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                System.out.println(isNetworkConnected());
//                Toast.makeText(getApplicationContext(), isNetworkConnected()?"True":"False", Toast.LENGTH_LONG).show();
                if (isNetworkConnected()) {
                    Snackbar snackbar = Snackbar.make(findViewById(R.id.layout), "Connected", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(DEBUG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(DEBUG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(DEBUG, "onPause");
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.d(DEBUG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(DEBUG, "onDestroy");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(DEBUG, "onActivityResult");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(DEBUG, "onRestart");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(DEBUG, "onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(DEBUG, "onRestoreInstanceState");
    }
}
